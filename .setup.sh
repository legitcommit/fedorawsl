#!/bin/bash
PACKAGES=(
    "util-linux"
    "passwd"
    "wget"
    "openssh"
    "iputils"
    "unzip"
    "tar"
    "git"
    "ncurses"
    "nano"
    "zsh")

USERNAME=""

if !(grep -Fxq "max_parallel_downloads" /etc/dnf/dnf.conf)
then
    echo 'max_parallel_downloads=10' >> /etc/dnf/dnf.conf
fi

if !(grep -Fxq "fastestmirror" /etc/dnf/dnf.conf)
then
    echo 'fastestmirror=True' >> /etc/dnf/dnf.conf
fi

dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

dnf update -y
dnf upgrade -y

dnf install -y "${PACKAGES[@]}"

setcap cap_net_raw+p /bin/ping

read -p "Enter new username: " USERNAME

useradd -s /bin/zsh "$USERNAME"
passwd "$USERNAME"

usermod -aG wheel "$USERNAME"

su - "$USERNAME"
cd ~
